<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,600,700,700i" rel="stylesheet">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyseventeen' ); ?></a>

	<header class="site-header" >

		<div class="wrap">
			<div class="mobile-icon">
				<span></span>
				<span></span>
				<span></span>
			</div>
			<div class="tag">WHERE <strong>UNIQUE</strong> IS COMMONPLACE</div>
			<div class="header-right">
				<span>(312) 326-2121</span>
				<a href="<?php bloginfo('url') ?>/agents">FIND AN AGENT</a>
			</div>
		
		</div>

	</header><!-- #masthead -->

	
	
	<?php if ( !is_front_page() && !is_home() ) { ?>
	<section class="inner-nav">
    	<div class="wrap">
				<div class="logo-main"><a href="<?php bloginfo('url') ?>"><img src="<?php bloginfo('template_url') ?>/assets/images/logo-c21sgr-white.png" alt="Century 21 SGR Chicago Real Estate"></a></div>
				<ul>
					<li><a href="<?php bloginfo('url') ?>/agents">Agent Profiles</a></li>
					<li><a href="<?php bloginfo('url') ?>/neighborhood-pricing">Neighborhood Pricing</a></li>
					<li><a href="<?php bloginfo('url') ?>/about-us">About Us</a></li>
					<li><a href="<?php bloginfo('url') ?>/rental-package">Rental Package</a></li>
					<li><a href="<?php bloginfo('url') ?>/agent-login">Agent Login</a></li>
				</ul>
			</div>
	</section>
	<?php } ?>

	<div class="site-content-contain">
		<div id="content" class="site-content">
