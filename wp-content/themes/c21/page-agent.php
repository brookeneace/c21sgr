<?php
/**
 * Template Name: Agent Profile
 * Template Post Type: agents
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			
			
			<div id="agent-wrap">
				<?php
				while ( have_posts() ) : the_post();
	
					get_template_part( 'template-parts/page/content', 'page-agent' );
	
	
				endwhile; // End of the loop.
				?>
			</div>
			
			<div class="reviews">
				
				
				
				<?php

				// check if the repeater field has rows of data
				if( have_rows('reviews') ):?>
				 <h2><?php the_title();?> Reviews</h2>
				    <h3><?php the_field('number_of_reviews'); ?> Reviews</h3>
				    <ul class="reviews">
				 	
				    <?php while ( have_rows('reviews') ) : the_row();
					    $link = get_sub_field_object('review_site');
					    $value = $link['value'];
				    ?>
				    
				   
					<li class="<?php echo $value; ?>">
						<blockquote>
						  <?php the_sub_field('review_content');?>
						</blockquote>
					</li>
				
				    <?php endwhile;?>
				    
				    </ul>
				
				
				<div class="external-links">
				<a href="<?php the_field('real_satisfied_url'); ?>" target="_blank">View more on Real Satisfied <i class="fas fa-external-link-alt"></i></a> <a href="<?php the_field('zillow_url'); ?>" target="_blank">View more on Zillow <i class="fas fa-external-link-alt"></i></a></div>
				
				<?php else :
				
				    // no rows found
				
				endif;
				
				?>
				
			</div>
			

			
			

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer();
