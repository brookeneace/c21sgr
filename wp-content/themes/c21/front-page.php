<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<div id="home-top-search">
			<div class="fade"></div>
			<section class="inner-nav home-inner">
				<div class="wrap">
					<div class="logo-main"><a href="<?php bloginfo('url') ?>"><img src="<?php bloginfo('template_url') ?>/assets/images/logo-c21sgr-white.png" alt="Century 21 SGR Chicago Real Estate"></a></div>
					<ul>
						<li><a href="<?php bloginfo('url') ?>/agents">Agent Profiles</a></li>
						<li><a href="<?php bloginfo('url') ?>/neighborhood-pricing">Neighborhood Pricing</a></li>
						<li><a href="<?php bloginfo('url') ?>/about-us">About Us</a></li>
						<li><a href="<?php bloginfo('url') ?>/rental-package">Rental Package</a></li>
						<li><a href="<?php bloginfo('url') ?>/agent-login">Agent Login</a></li>
					</ul>
				</div>
			</section>
			<div class="wrap">
				<div id="prop-search">
					<div class="sale-rent">
						<span class="active">Buy</span><span>Rent</span>
					</div>
					
					<form>
					<input type="text" placeholder="Enter a neighborhood, city, address or Zip code">
					<select>
						<option>Property Type</option>
						<option>House</option>
						<option>Condo</option>
						<option>Lot/Land</option>
						<option>Office</option>
						<option>Commercial</option>
						
					</select>
					
					<select>
						<option>Price</option>
						<option>$100,000+</option>
						<option>$200,000+</option>
						<option>$300,000+</option>
						<option>$400,000+</option>
						<option>$500,000+</option>
						<option>$600,000+</option>
						<option>$700,000+</option>
						<option>$800,000+</option>
						<option>$900,000+</option>
						<option>$1,000,000+</option>
						
					</select>
					
					<select>
						<option>Bedrooms</option>
						<option>1+</option>
						<option>2+</option>
						<option>3+</option>
						<option>4+</option>
						<option>5+</option>
						
					</select>
					<input type="submit" value="Search">
					</form>
				</div>
			</div>
		</div>
		
			<div id="agents-home">
			
			<div class="agents-wrap">
				<h1>Agent Reviews</h1>
				<div class="wrap">
					<ul class="agents">
						
						
						<?php
						
						        // The Arguments
						        $args = array(
						            'post_type' => 'agents', 
						            'showposts' => 100,
                                    'orderby'=>'title','order'=>'ASC',
						            'meta_query' => array(
									    array(
									        'key' => 'reviews',
									        'value'   => array(''),
									        'compare' => 'NOT IN'
									    )
									)
						           
						        );  
						
						        // The Query
						        $the_query = new WP_Query( $args ); ?>
						
						        <?php
						
						        // If we have the posts...
						        if ( $the_query->have_posts() ) : ?>
						
						        <!-- Start the loop the loop --> 
						            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						
						                <li><a href="<?php the_permalink(); ?>">
							                <div class="agent-pic">
								                <?php  if(has_post_thumbnail()){
									                  the_post_thumbnail();
									             } ?>
								<div class="agent-hover"><h4>View Profile</h4></div>
								</div>
								<h3><?php the_title(); ?></h3></a>
								<p style="margin-bottom: 0;"><?php the_field('number_of_reviews'); ?> REVIEWS</p>
								<p><?php the_field('primary_office_name'); ?></p>
								<a href="tel:<?php the_field('phone'); ?>"><i class="fas fa-phone"></i></a>
								<a href="mailto:<?php the_field('email'); ?>"><i class="fas fa-envelope"></i></a>
								<?php if( get_field('website') ): ?>
									<a href="<?php the_field('website'); ?>"><i class="fas fa-laptop"></i></a>
								<?php endif; ?>
								<p><span><?php echo get_the_term_list( $post->ID, 'languages', 'Additional Languages Spoken: ', ', ', '' ); ?> </span></p>
								</li>
						
						            <?php endwhile; endif; // end of the loop. ?>
						
						        <?php wp_reset_postdata(); ?>
						
					</ul>
					
					
				</div>
				<p><a href="<?php bloginfo('url') ?>/agents"><button>View All Agents</button></a></p>
			</div>
			
			
		</div>
		
		
		<div id="featured-listings">
			<h1>Featured listings</h1>
			<div class="wrap">
				
				<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/page/content', 'page-home' );


			endwhile; // End of the loop.
			?>
			
			</div>
		</div>
		
		
	
		
		
		<div id="neighborhoods">
				<h1>Neighborhood Profiles</h1>
				
				<div class="wrap">
					<article>
						<div class="article-image"><img src="<?php bloginfo('template_url')?>/assets/images/Neighborhood-LincolnPark.jpg"></div>
						<h3>Lincoln Park</h3>
					</article>
					<article>
						<div class="article-image"><img src="<?php bloginfo('template_url')?>/assets/images/Neighborhood-Pilsen.jpg"></div>
						<h3>Pilsen</h3>
					</article>
					<article>
						<div class="article-image"><img src="<?php bloginfo('template_url')?>/assets/images/Neighborhood-SouthLoop.jpg"></div>
						<h3>South Loop</h3>
					</article>
				</div>
	
			<div class="view-all-neighborhoods">
				<div class="wrap">
				<h2>View all Chicago neighborhoods</h2>
				<ul>
					<li><h4>North</h4>

						<?php
						
						        // The Arguments
						        $args = array(
						            'post_type' => 'neighborhoods', 
						            'tax_query' => array(
								        array(
								            'taxonomy' => 'neighborhoods_categories',
								            'field'    => 'slug',
								            'terms' => 'north' //if field is ID you can reference by cat/term number
								        )
								    )
						        );  
						
						        // The Query
						        $the_query = new WP_Query( $args ); ?>
						
						        <?php
						
						        // If we have the posts...
						        if ( $the_query->have_posts() ) : ?>
						
						        <!-- Start the loop the loop --> 
						            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						
						                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						
						            <?php endwhile; endif; // end of the loop. ?>
						
						        <?php wp_reset_postdata(); ?>
						
						
					</li>
					<li><h4>Northwest</h4>
					<?php
						
						        // The Arguments
						        $args = array(
						            'post_type' => 'neighborhoods', 
						            'tax_query' => array(
								        array(
								            'taxonomy' => 'neighborhoods_categories',
								            'field'    => 'slug',
								            'terms' => 'northwest' //if field is ID you can reference by cat/term number
								        )
								    )
						        );  
						
						        // The Query
						        $the_query = new WP_Query( $args ); ?>
						
						        <?php
						
						        // If we have the posts...
						        if ( $the_query->have_posts() ) : ?>
						
						        <!-- Start the loop the loop --> 
						            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						
						                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						
						            <?php endwhile; endif; // end of the loop. ?>
						
						        <?php wp_reset_postdata(); ?>

					</li>
					<li><h4>South</h4>
					<?php
						
						        // The Arguments
						        $args = array(
						            'post_type' => 'neighborhoods', 
						            'tax_query' => array(
								        array(
								            'taxonomy' => 'neighborhoods_categories',
								            'field'    => 'slug',
								            'terms' => 'south' //if field is ID you can reference by cat/term number
								        )
								    )
						        );  
						
						        // The Query
						        $the_query = new WP_Query( $args ); ?>
						
						        <?php
						
						        // If we have the posts...
						        if ( $the_query->have_posts() ) : ?>
						
						        <!-- Start the loop the loop --> 
						            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						
						                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						
						            <?php endwhile; endif; // end of the loop. ?>
						
						        <?php wp_reset_postdata(); ?>

					</li>
					<li><h4>Southwest</h4>
					<?php
						
						        // The Arguments
						        $args = array(
						            'post_type' => 'neighborhoods', 
						            'tax_query' => array(
								        array(
								            'taxonomy' => 'neighborhoods_categories',
								            'field'    => 'slug',
								            'terms' => 'southwest' //if field is ID you can reference by cat/term number
								        )
								    )
						        );  
						
						        // The Query
						        $the_query = new WP_Query( $args ); ?>
						
						        <?php
						
						        // If we have the posts...
						        if ( $the_query->have_posts() ) : ?>
						
						        <!-- Start the loop the loop --> 
						            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						
						                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						
						            <?php endwhile; endif; // end of the loop. ?>
						
						        <?php wp_reset_postdata(); ?>

					</li>
					
					<li><h4>Suburbs – North</h4>
			
					</li>
					<li><h4>Suburbs – Northwest</h4>
					
					</li>
					<li><h4>Suburbs – West</h4>
					
					</li>
					<li><h4>Suburbs – South</h4>
					
					</li>
				</ul>
				</div>
			</div>
		</div>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();
