jQuery(document).ready(function ($) {

	console.log('ready');
	
	$('ul.reviews').each(function() {
	    var $list = $(this);
	    $list.after('<div class="button-wrapper"><button class="more_less">More</button></div>')
	   $list.find('li:gt(1)').hide();
	});


	$('.more_less').click(function() {
	    var $btn = $(this)
	    $btn.parent().prev().find('li:gt(1)').slideToggle();    
	    $btn.text($btn.text() == 'More' ? 'Less' : 'More');    
	});
	
	$(function(){
      // bind change event to select
      $('#languages').on('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });
    
   


});