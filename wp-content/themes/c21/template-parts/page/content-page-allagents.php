<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php twentyseventeen_edit_link( get_the_ID() ); ?>
	</header><!-- .entry-header -->
	<div class="breadcrumb">
		
		<p>We are a multicultural real estate company.</p>
		Filter by: <select name="languages" id="languages">
						<option>Choose Language</option>
					<?php  $terms = get_terms('languages', 'parent=0');
					    $count = count($terms);
					    if ( $count > 0 ){
					        foreach ( $terms as $term ) {
					            echo '<option value="http://localhost:8888/c21sgr/languages/' . $term->name . '">' . $term->name . '</option>';
					        }
					    }
					?>
					</select>
	</div>
		<?php
			the_content();
		?>
	

</article><!-- #post-## -->
