<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>


	
	
				
						
						   <li><a href="<?php the_permalink(); ?>">
							                <div class="agent-pic">
								                <?php  if(has_post_thumbnail()){
									                  the_post_thumbnail();
									             } ?>
								<div class="agent-hover"><h4>View Profile</h4></div>
								</div>
								<h3><?php the_title(); ?></h3></a>
								<p><?php the_field('primary_office_name'); ?></p>
								<i class="fas fa-phone"></i>
								<i class="fas fa-envelope"></i>
								<p><span><?php echo get_the_term_list( $post->ID, 'languages', 'Additional Languages Spoken: ', ', ', '' ); ?> </span></p>

								
								</li>
						
						           			
					
			


<!--
		<?php
			the_content();
		?>
-->
	

